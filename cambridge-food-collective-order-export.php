<?php
/*
Plugin Name: Cambridge Food Collective Orders Export
Plugin URI: http://www.calliaweb.co.uk
Description: Adds Cambridge Food Collective orders export functionality
Version: 1.0.0
Author: Jo Waltham
Author URI: http://www.calliaweb.co.uk
*/

// if this file is called directly about
if ( ! defined('WPINC' )) {
	die;
}

// create top level admin menu
add_action('admin_menu', 'cfc_orders_export_admin_menu');
function cfc_orders_export_admin_menu() {

  add_menu_page('CFC Orders Export', 'CFC Orders Export', 10, 'cfc_orders_export', 'cfc_orders_export_admin_page');

}


function cfc_orders_export_admin_page() {

	?>
	<div class="wrap">

        <h2>Cambridge Food Collective Orders Export to CSV</h2>
        <p>Click export to generate a csv file of all orders with status of processing or on hold.</p>

      	<form method="post" id="export-form" action="">
            <?php submit_button('Export', 'primary', 'download_csv' ); ?>
        </form>

    </div>
    <?php
}


add_action('admin_init', 'cfc_orders_export_admin_init');
function cfc_orders_export_admin_init() {

	global $plugin_page;

	if ( isset($_POST['download_csv']) && $plugin_page == 'cfc_orders_export' ) {
	   	cfc_generate_orders_export();
	    die();

	}

}

function cfc_generate_orders_export() {

	// output headers so that the file is downloaded rather than displayed
	header('Content-Type: text/csv; charset=utf-8');

	// set file name with current date
	header('Content-Disposition: attachment; filename=cfc-orders-export-' . date('Y-m-d') . '.csv');

	// create a file pointer connected to the output stream
	$output = fopen('php://output', 'w');

	// set the column headers for the csv
	$headings = array( 'Order Number', 'Order Date', 'Customer', 'Producer', 'Product', 'Variation', 'Qty', 'Sub Total', 'Supplier Amount', 'Commission Type', 'Commission Rate Per Item', 'CFC Amount' );
	fputcsv( $output, $headings );

	$args = array(
		'post_type'			=> 'shop_order',
		'post_status' 		=> 'publish',
	    'posts_per_page' 	=> -1,
	    'tax_query' 		=> array(
	    	array(
	    		'taxonomy' => 'shop_order_status',
	    		'field' => 'slug',
	    		'terms' => array( 'processing', 'on-hold' )
	    	)
	    )
    );

    $wc_vendor_options = get_option( 'wc_prd_vendor_options' );
	if( $wc_vendor_options ) {
		$default_commission_type = $wc_vendor_options[ 'commission_type' ]; // returns percent, fixed, fixed_fee, percent_fee
		$default_commission_percent = $wc_vendor_options[ 'commission_percent' ];
		$default_commission_fixed = $wc_vendor_options[ 'commission_amount' ];
		$default_commission_fee = $wc_vendor_options[ 'commission_fee' ];
	}

	$loop = new WP_Query( $args );
	$vendor_product_qty = array();

	//loop through orders
	while ( $loop->have_posts() ) : $loop->the_post();

		$order = new WC_Order($loop->post->ID);
		$customer = get_userdata( $order->user_id );
		$customer_name = $customer->first_name . ' ' . $customer->last_name;
		if( empty( $customer->first_name ) && empty( $customer->last_name ) ) {
			$customer_name = $customer->user_login;
		}

		// loop through each item in this order
		foreach ($order->get_items() as $item) {
			$commission = '';
			$commission_rate = '';
			$commission_type = '';
			$cfc_amount = '';

			//$commission = WCV_Commission::get_commission_rate( $item['product_id'] );
			//$commission = WCV_Commission::calculate_commission( $item[ 'line_subtotal' ], $item[ 'product_id' ], $order, $item[ 'qty' ] );

			// Get product commission details
			$product_commission_type = get_post_meta( $item[ 'product_id' ], 'wcv_commission_type', true );
			$product_commission_percent = get_post_meta( $item[ 'product_id' ], 'wcv_commission_percent', true );
			$product_commission_fixed = get_post_meta( $item[ 'product_id' ], 'wcv_commission_amount', true );

			// if we have a production commission type then what kind of commission - fixed or percentage?
			if( !empty( $product_commission_type ) ) {
				if( 'percent' === $product_commission_type ) {
					$commission = round( $item['line_subtotal'] * ( $product_commission_percent / 100 ), 2);
					$commission_rate = $product_commission_percent;
					$commission_type = 'product percentage';
				} else if ( 'fixed' === $product_commission_type ) {
					$commission = $item['qty'] * $product_commission_fixed;
					$commission_rate = $product_commission_fixed;
					$commission_type = 'product fixed';
				} else {
					$commission = 'manual calculation required';
					$commission_rate = 'tbc';
					$commission_type = 'product' . $product_commission_type;
				}
			} else if( empty( $product_commission_type ) ) { // if no product commission details then vendor or site default commission rate applies
				$vendor_id = get_post_field( 'post_author', $item[ 'product_id' ] );
				$vendor_commission_percentage = get_user_meta( $vendor_id, 'pv_custom_commission_rate', true );

				if( $vendor_commission_percentage ) {
					$commission_type = 'producer percentage';
					$commission = round( $item['line_subtotal'] * ( $vendor_commission_percentage / 100 ), 2);
					$commission_rate = $vendor_commission_percentage;
				}
				else { // site commission applies
					if( 'percent' === $default_commission_type ) {
						$commission_type = 'site percentage';
						$commission_rate = $default_commission_percent;
						$commission = round( $item['line_subtotal'] * ( $default_commission_percent / 100 ), 2);
					} else {
						$commission_type = 'site ' . $default_commission_type;
						$commission_rate = 'tbc';
						$commission = 'manual calculation required';
					}
				}
			}

			if( !empty( $commission ) ) {
				$cfc_amount = round( $item['line_subtotal'] - $commission, 2 );
			}

			// Variation Name
			$variation_title = '';
			if( 0 <> $item[ 'variation_id' ] ) {
				$variation = wc_get_product( $item[ 'variation_id' ] );
				$variation_title = $variation->get_formatted_variation_attributes( true );
			}

			// Output to csv
			$row = array( $loop->post->ID, $order->order_date, $customer_name, $item['Producer'], $item['name'],  $variation_title, $item['qty'], $item['line_subtotal'], $commission, $commission_type, $commission_rate, $cfc_amount );
			fputcsv($output, $row);
    	}

	endwhile;

	wp_reset_postdata();

}